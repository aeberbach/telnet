Telnet
======

Telnet (VT220) client from which Nethack might have come. This works reasonably well on an iPad.
I was able to connect to Nethack telnet servers and watch games on progress in some graphics
modes so rendering and character transfer is pretty functional. There are color bugs and character
bugs still. To change the telnet destination there's no UI, edit the source.

I always expected someone would produce a better hard-keyboard-only Nethack for iOS before
I got close but it didn't happen and the reason is probably GPL. That's one problem.

The other problem is that Apple does not expose all of the keys required. That's why you see 
an extra line above the on-screen keyboard in apps like Prompt (excellent iOS SSH client from
Panic, Inc.) and it remains even when a hard keyboard is attached. CTRL, ESC etc. are vital
terminal keyboard commands for Nethack and there is no way to get them.

If you want a state machine for terminal emulation this might be interesting. If you just need
an iOS terminal, buy Prompt.
